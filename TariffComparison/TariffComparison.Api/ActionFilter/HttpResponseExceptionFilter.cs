﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using TariffComparison.Services.Dto;

namespace TariffComparison.Api.ActionFilter
{
    public class HttpResponseExceptionFilter : IActionFilter, IOrderedFilter
    {
        public int Order { get; set; } = int.MaxValue - 10;
        
        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context?.Exception is CustomException exception)
            {
                context.Result = new ObjectResult(exception.CustomErrorDetails)
                {
                    StatusCode = exception.Status,
                };
                context.ExceptionHandled = true;
            }
           
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            
        }
    }
}
