﻿using Microsoft.AspNetCore.Mvc;
using TariffComparison.Services.Interfaces;

namespace TariffComparison.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TariffComparisonController : Controller
    {
        private readonly ITariffComparisonService tariffComparisonService;

        public TariffComparisonController(ITariffComparisonService tariffComparisonService)
        {
            this.tariffComparisonService = tariffComparisonService;
        }
        [HttpGet("{kwhPerYear}")]
        public IActionResult Get(int kwhPerYear)
        {            
            return Ok(tariffComparisonService.GetTariffComparison(kwhPerYear));
        }
    }
}