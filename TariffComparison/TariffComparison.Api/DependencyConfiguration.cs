﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using TariffComparison.Services;
using TariffComparison.Services.Configurations;
using TariffComparison.Services.Configurations.Interfaces;
using TariffComparison.Services.Interfaces;
using TariffComparison.Services.TariffRules;
using TariffComparison.Services.TariffRules.Interfaces;

namespace TariffComparison.Api
{
    public static class DependencyConfiguration
    {
        public static void RegisterConfiguration(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<BasicTariffConfiguration>(configuration?.GetSection("TariffRules:Basic"));
            services.AddSingleton<IBasicTariffConfiguration>(sp =>
                sp.GetRequiredService<IOptions<BasicTariffConfiguration>>().Value);

            services.Configure<PackagedTariffConfiguration>(configuration?.GetSection("TariffRules:Packaged"));
            services.AddSingleton<IPackagedTariffConfiguration>(sp =>
                sp.GetRequiredService<IOptions<PackagedTariffConfiguration>>().Value);

            services.Configure<CommonTariffConfiguration>(configuration?.GetSection("TariffRules"));
            services.AddSingleton<ICommonTariffConfiguration>(sp =>
                sp.GetRequiredService<IOptions<CommonTariffConfiguration>>().Value);            
        }
        public static void RegisterServices(IServiceCollection services)
        {                        
            services.AddScoped<ITariffRule, BasicTariff>();
            services.AddScoped<ITariffRule, PackagedTariff>();
            services.AddScoped<ITariffComparisonService, TariffComparisonService>();            
        }        
    }
}
