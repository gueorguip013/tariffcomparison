﻿using Moq;
using Serilog;

namespace TariffComparison.Services.UnitTest.Moqs
{
    public static class MockLog
    {
        private static Mock<ILogger> loggerMock = new Mock<ILogger>();

        public static ILogger LoggerMock { get; } = loggerMock.Object;
    }
}
