﻿using Shouldly;
using System.Collections.Generic;
using System.Linq;
using TariffComparison.Services.Configurations;
using TariffComparison.Services.TariffRules;
using TariffComparison.Services.TariffRules.Interfaces;
using TariffComparison.Services.UnitTest.Moqs;
using Xunit;

namespace TariffComparison.Services.UnitTest.TariffComparisonServiceTest
{

    public class TariffComparisonServiceTest
    {
        private CommonTariffConfiguration GetCommonConfiguration() { 
            return new CommonTariffConfiguration { Currency = "€", Period = "Yearly" };
        }

        private PackagedTariff GetPackagedTariff() {
            var packagedConfiguration = new PackagedTariffConfiguration { AdditionalCents = 30, BaseTariff = 800, KwhLimitValue = 4000 };
            
            return new PackagedTariff(GetCommonConfiguration(), packagedConfiguration, MockLog.LoggerMock);
        }
        private BasicTariff GetBasicTariff()
        {
            var basicConfiguration = new BasicTariffConfiguration { BaseCost = 5, CostKwhCents = 22 };
            
            return new BasicTariff(GetCommonConfiguration(), basicConfiguration, MockLog.LoggerMock);
        }

        [Fact]
        public void GetListOfComparationTariffsWith3500Kwh()
        {            
            var ruleList = new List<ITariffRule> {
                GetBasicTariff(),GetPackagedTariff()
            };

            var tarriffComparisonServices = new TariffComparisonService(ruleList, MockLog.LoggerMock);

            var result = tarriffComparisonServices.GetTariffComparison(3500);

            result.Count().ShouldBe(2);            

            decimal min = 0;
            foreach (var item in result) {
                item.AnnualCost.ShouldBeGreaterThanOrEqualTo(min);
                min = item.AnnualCost;
            }
        }

        [Fact]
        public void GetListOfComparationTariffsWith6000Kwh()
        {
            var ruleList = new List<ITariffRule> {
                GetBasicTariff(),GetPackagedTariff()
            };

            var tarriffComparisonServices = new TariffComparisonService(ruleList, MockLog.LoggerMock);

            var result = tarriffComparisonServices.GetTariffComparison(3500);

            result.Count().ShouldBe(2);

            decimal min = 0;
            foreach (var item in result)
            {
                item.AnnualCost.ShouldBeGreaterThanOrEqualTo(min);
                min = item.AnnualCost;
            }
        }
    }
}
