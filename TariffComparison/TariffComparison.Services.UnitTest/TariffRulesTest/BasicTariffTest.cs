﻿using Shouldly;
using TariffComparison.Services.Configurations;
using TariffComparison.Services.Dto;
using TariffComparison.Services.TariffRules;
using TariffComparison.Services.UnitTest.Moqs;
using Xunit;

namespace TariffComparison.Services.UnitTest.TariffRulesTest
{
    public class BasicTariffTest
    {
        private BasicTariff GetBasicTariffObject(BasicTariffConfiguration config = null)
        {
            var defaultConfiguration = new BasicTariffConfiguration { BaseCost = 5, CostKwhCents = 22 };
            var commonConfiguration = new CommonTariffConfiguration { Currency = "€", Period = "Yearly" };

            return new BasicTariff(commonConfiguration, config ?? defaultConfiguration, MockLog.LoggerMock);
        }

        private void CheckGeneralData(Tariff tariff)
        {
            tariff.Currency.ShouldBe("€");
            tariff.Period.ShouldBe("Yearly");
            tariff.TariffName.ShouldBe("Basic Electricity Tariff");
        }

        [Fact]
        public void Check3500KwhPerYearUsingBasicTariffRule()
        {            
            var tariff = GetBasicTariffObject().GetTariff(3500);
            tariff.AnnualCost.ShouldBe(830);
            tariff.AnnualCostFormatted.ShouldBe("€830.00/Yearly");
            CheckGeneralData(tariff);
        }

        [Fact]
        public void Check3501KwhPerYearUsingBasicTariffRule()
        {
            var tariff = GetBasicTariffObject().GetTariff(3501);
            tariff.AnnualCost.ShouldBe<decimal>(830.22m);
            tariff.AnnualCostFormatted.ShouldBe("€830.22/Yearly");
            CheckGeneralData(tariff);
        }

        [Fact]
        public void CheckMissingConfigurationBasicTariffRule()
        {
            var configuration = new BasicTariffConfiguration();
                        
            var ex = Should.Throw<CustomException>(() => GetBasicTariffObject(configuration).GetTariff(3500));
            ex.CustomErrorDetails.Code.ShouldBe(CustomExceptionTypes.UnExpectedError.CustomErrorDetails.Code);
            ex.CustomErrorDetails.Message.ShouldBe(CustomExceptionTypes.UnExpectedError.CustomErrorDetails.Message);
            ex.Status.ShouldBe(CustomExceptionTypes.UnExpectedError.Status);
        }

        [Fact]
        public void CheckNegativeNumbersInConfigurationUsingBasicTariffRule()
        {
            var configuration = new BasicTariffConfiguration { BaseCost = -5, CostKwhCents = -22 };
                        
            var ex = Should.Throw<CustomException>(() => GetBasicTariffObject(configuration).GetTariff(3500));
            ex.CustomErrorDetails.Code.ShouldBe(CustomExceptionTypes.UnExpectedError.CustomErrorDetails.Code);
            ex.CustomErrorDetails.Message.ShouldBe(CustomExceptionTypes.UnExpectedError.CustomErrorDetails.Message);
            ex.Status.ShouldBe(CustomExceptionTypes.UnExpectedError.Status);
        }
    }
}
