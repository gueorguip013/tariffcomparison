﻿using Shouldly;
using TariffComparison.Services.Configurations;
using TariffComparison.Services.TariffRules;
using Xunit;
using TariffComparison.Services.UnitTest.Moqs;
using TariffComparison.Services.Dto;

namespace TariffComparison.Services.UnitTest.TariffRulesTest
{
    
    public class PackagedTariffTest
    {

        private PackagedTariff GetPackagedTariffObject(PackagedTariffConfiguration config = null) {
            var defaultConfiguration = new PackagedTariffConfiguration { AdditionalCents = 30, BaseTariff = 800, KwhLimitValue = 4000 };
            var commonConfiguration = new CommonTariffConfiguration { Currency = "€", Period = "Yearly" };

            return new PackagedTariff(commonConfiguration, config ?? defaultConfiguration, MockLog.LoggerMock);
        }

        private void CheckGeneralData(Tariff tariff)
        {
            tariff.Currency.ShouldBe("€");
            tariff.Period.ShouldBe("Yearly");
            tariff.TariffName.ShouldBe("Packaged tariff");
        }

        [Fact]
        public void Check3500KwhPerYearUsingPackagedTariffRule()
        {                        
            var tariff = GetPackagedTariffObject().GetTariff(3500);
            tariff.AnnualCost.ShouldBe(800);
            tariff.AnnualCostFormatted.ShouldBe("€800.00/Yearly");
            CheckGeneralData(tariff);
        }

        [Fact]
        public void Check4500KwhPerYearUsingPackagedTariffRule()
        {                        
            var tariff = GetPackagedTariffObject().GetTariff(4500);
            tariff.AnnualCost.ShouldBe(950);
            tariff.AnnualCostFormatted.ShouldBe("€950.00/Yearly");
            CheckGeneralData(tariff);

        }

        [Fact]
        public void Check6000KwhPerYearUsingPackagedTariffRule()
        {                        
            var tariff = GetPackagedTariffObject().GetTariff(6000);
            tariff.AnnualCost.ShouldBe(1400);
            tariff.AnnualCostFormatted.ShouldBe("€1400.00/Yearly");
            CheckGeneralData(tariff);

        }

        [Fact]
        public void CheckInvalidConfigurationUsingPackagedTariffRule()
        {
            var configuration = new PackagedTariffConfiguration { AdditionalCents = -5, BaseTariff = -5, KwhLimitValue = -2 };
            
            var ex = Should.Throw<CustomException>(() => GetPackagedTariffObject(configuration).GetTariff(3500));
            ex.CustomErrorDetails.Code.ShouldBe(CustomExceptionTypes.UnExpectedError.CustomErrorDetails.Code);
            ex.CustomErrorDetails.Message.ShouldBe(CustomExceptionTypes.UnExpectedError.CustomErrorDetails.Message);
            ex.Status.ShouldBe(CustomExceptionTypes.UnExpectedError.Status);

        }

        [Fact]
        public void CheckMissingConfigurationUsingPackagedTariffRule()
        {
            var configuration = new PackagedTariffConfiguration();
            
            var ex = Should.Throw<CustomException>(() => GetPackagedTariffObject(configuration).GetTariff(3500));
            ex.CustomErrorDetails.Code.ShouldBe(CustomExceptionTypes.UnExpectedError.CustomErrorDetails.Code);
            ex.CustomErrorDetails.Message.ShouldBe(CustomExceptionTypes.UnExpectedError.CustomErrorDetails.Message);
            ex.Status.ShouldBe(CustomExceptionTypes.UnExpectedError.Status);

        }
    }
}
