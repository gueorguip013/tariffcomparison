﻿using System.Collections.Generic;
using TariffComparison.Services.Dto;
using TariffComparison.Services.Interfaces;
using TariffComparison.Services.TariffRules.Interfaces;
using Serilog;
using System.Linq;

namespace TariffComparison.Services
{
    public class TariffComparisonService: ITariffComparisonService
    {
        private IEnumerable<ITariffRule> tariffRules;        
        private ILogger logger;
        public TariffComparisonService(IEnumerable<ITariffRule> tariffRules,             
            ILogger logger)
        {
            this.tariffRules = tariffRules;            
            this.logger = logger;
        }
        public IEnumerable<Tariff> GetTariffComparison(int kwhPerYear) 
        {
            logger.Information($"Get tariff comparison list.");

            return new List<Tariff>(tariffRules.Select(t => t.GetTariff(kwhPerYear)).OrderBy(t => t.AnnualCost));
                                    
        }
    }
}
