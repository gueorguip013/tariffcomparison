﻿using System.Text.Json.Serialization;

namespace TariffComparison.Services.Dto
{
    public class Tariff
    {        
        [JsonPropertyName("tariffName")]
        public string TariffName { get; set; }        
        [JsonPropertyName("annualCost")]
        public string AnnualCostFormatted => string.Format("{0}{1}/{2}",Currency, System.Math.Round(AnnualCost, 2).ToString("#.00"), Period);
        [JsonIgnore]
        public decimal AnnualCost { get; set; }
        [JsonIgnore]
        public string Currency { get; set; }        
        [JsonIgnore]
        public string Period { get; set; }
    }
}
