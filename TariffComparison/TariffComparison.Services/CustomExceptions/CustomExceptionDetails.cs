﻿using System.Text.Json.Serialization;

namespace TariffComparison.Services.Dto
{
    public class CustomExceptionDetails
    {
        [JsonPropertyName("errorCode")]        
        public int Code { get; set; }
        [JsonPropertyName("errorMessage")]
        public string Message { get; set; }
    }
}
