﻿namespace TariffComparison.Services.Dto
{
    public static class CustomExceptionTypes
    {
        public static CustomException UnExpectedError = new CustomException { 
            CustomErrorDetails = new CustomExceptionDetails { 
                Code = 10000, 
                Message = "An unexpected error has occurred." }, 
            Status = 400 };
    }

}
