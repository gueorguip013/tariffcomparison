﻿using System;

namespace TariffComparison.Services.Dto
{
    public class CustomException : Exception
    {
        public int Status { get; set; } = 500;
        public CustomExceptionDetails CustomErrorDetails { get; set; }
    }            
}
