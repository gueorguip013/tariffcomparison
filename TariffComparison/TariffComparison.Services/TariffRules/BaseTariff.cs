﻿using TariffComparison.Services.Configurations.Interfaces;
using TariffComparison.Services.Dto;
using TariffComparison.Services.TariffRules.Interfaces;

namespace TariffComparison.Services.TariffRules
{
    public abstract class BaseTariff: ITariffRule
    {        
        protected string Currency { get; }
        protected string Period { get; }        
        protected abstract string TariffName { get; }        
        protected abstract decimal GetAnnualCost(int kwhPerYear);
        protected abstract void CheckConfiguration();
        public BaseTariff(ICommonTariffConfiguration commonTariffConfiguration)
        {
            Currency = commonTariffConfiguration.Currency;
            Period = commonTariffConfiguration.Period;
        }
        protected decimal ConvertCentsToUnits(int cents) {
            return cents / 100.0m;
        }

        public virtual Tariff GetTariff(int kwhPerYear)
        {
            return new Tariff
            {
                AnnualCost = GetAnnualCost(kwhPerYear),
                TariffName = TariffName,
                Currency = Currency,
                Period = Period
            };
        }
    }
}
