﻿using TariffComparison.Services.Configurations.Interfaces;
using Serilog;
using TariffComparison.Services.Dto;

namespace TariffComparison.Services.TariffRules
{
    public class BasicTariff : BaseTariff
    {
        private readonly IBasicTariffConfiguration basicTariffConfiguration;        
        private const int MonthsOfYear = 12;
        private ILogger logger;
        protected override string TariffName { get; } = "Basic Electricity Tariff";
        
        public BasicTariff(ICommonTariffConfiguration commonTariffConfiguration,
            IBasicTariffConfiguration basicTariffConfiguration, ILogger logger) :base(commonTariffConfiguration)
        {
            this.basicTariffConfiguration = basicTariffConfiguration;            
            this.logger = logger;
        }

        protected override void CheckConfiguration()
        {
            if (basicTariffConfiguration.BaseCost < 0
                || basicTariffConfiguration.CostKwhCents <= 0) 
            {
                logger.Error(string.Format("Invalid {0} rules configuration.", TariffName));
                throw CustomExceptionTypes.UnExpectedError;
            }                        
        }

        protected override decimal GetAnnualCost(int kwhPerYear)
        {
            CheckConfiguration();

            logger.Information($"Calculating {TariffName} cost.");

            return (basicTariffConfiguration.BaseCost * MonthsOfYear) + kwhPerYear 
                * ConvertCentsToUnits(basicTariffConfiguration.CostKwhCents);
        }
        
    }
}
