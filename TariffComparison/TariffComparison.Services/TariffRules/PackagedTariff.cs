﻿using Serilog;
using TariffComparison.Services.Configurations.Interfaces;
using TariffComparison.Services.Dto;

namespace TariffComparison.Services.TariffRules
{
    public class PackagedTariff : BaseTariff
    {
        private readonly IPackagedTariffConfiguration packagedTariffConfiguration;
        protected override string TariffName { get; } = "Packaged tariff";
        
        private ILogger logger;

        public PackagedTariff(ICommonTariffConfiguration commonTariffConfiguration, 
            IPackagedTariffConfiguration packagedTariffConfiguration, ILogger logger): base(commonTariffConfiguration)
        {
            this.packagedTariffConfiguration = packagedTariffConfiguration;
            this.logger = logger;
        }

        protected override void CheckConfiguration()
        {
            if (packagedTariffConfiguration.AdditionalCents <= 0 
                || packagedTariffConfiguration.KwhLimitValue <= 0 
                || packagedTariffConfiguration.BaseTariff <= 0)
            {
                logger.Error(string.Format("Invalid {0} rules configuration.", TariffName));
                throw CustomExceptionTypes.UnExpectedError;
            }
        }

        protected override decimal GetAnnualCost(int kwhPerYear)
        {
            CheckConfiguration();

            logger.Information($"Calculating {TariffName} cost.");

            if (kwhPerYear <= packagedTariffConfiguration.KwhLimitValue) return packagedTariffConfiguration.BaseTariff;

            return packagedTariffConfiguration.BaseTariff 
                + ((kwhPerYear - packagedTariffConfiguration.KwhLimitValue) 
                * ConvertCentsToUnits(packagedTariffConfiguration.AdditionalCents));
        }       
    }
}
