﻿using TariffComparison.Services.Dto;

namespace TariffComparison.Services.TariffRules.Interfaces
{
    public interface ITariffRule
    {
        Tariff GetTariff(int kwhPerYear);
    }
}
