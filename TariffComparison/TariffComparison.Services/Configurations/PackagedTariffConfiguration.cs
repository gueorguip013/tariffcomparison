﻿using TariffComparison.Services.Configurations.Interfaces;

namespace TariffComparison.Services.Configurations
{
    public class PackagedTariffConfiguration : IPackagedTariffConfiguration
    {
        public int KwhLimitValue { get; set; }
        public int AdditionalCents { get; set; }
        public int BaseTariff { get; set; }
    }
}
