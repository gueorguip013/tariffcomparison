﻿using TariffComparison.Services.Configurations.Interfaces;

namespace TariffComparison.Services.Configurations
{
    public class CommonTariffConfiguration : ICommonTariffConfiguration
    {
        public string Currency { get; set; }
        public string Period { get; set; }
    }
}
