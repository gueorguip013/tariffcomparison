﻿using TariffComparison.Services.Configurations.Interfaces;

namespace TariffComparison.Services.Configurations
{
    public class BasicTariffConfiguration: IBasicTariffConfiguration
    {
        public int BaseCost { get; set; }
        public int CostKwhCents { get; set; }
    }
}
