﻿namespace TariffComparison.Services.Configurations.Interfaces
{
    public interface IBasicTariffConfiguration
    {
        int BaseCost { get; set; }
        int CostKwhCents { get; set; }
    }
}
