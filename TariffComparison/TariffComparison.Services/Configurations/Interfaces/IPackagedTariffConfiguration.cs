﻿namespace TariffComparison.Services.Configurations.Interfaces
{
    public interface IPackagedTariffConfiguration
    {
        int KwhLimitValue { get; set; }
        int AdditionalCents { get; set; }
        int BaseTariff { get; set; }        
    }
}
