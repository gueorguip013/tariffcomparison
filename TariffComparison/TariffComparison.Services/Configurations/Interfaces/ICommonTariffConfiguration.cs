﻿namespace TariffComparison.Services.Configurations.Interfaces
{
    public interface ICommonTariffConfiguration
    {
        string Currency { get; set; }
        string Period { get; set; }        
    }
}
