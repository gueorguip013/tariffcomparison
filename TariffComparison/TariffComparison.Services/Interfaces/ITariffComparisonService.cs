﻿using System.Collections.Generic;
using TariffComparison.Services.Dto;

namespace TariffComparison.Services.Interfaces
{
    public interface ITariffComparisonService
    {
        IEnumerable<Tariff> GetTariffComparison(int kwhPerYear);
    }
}
